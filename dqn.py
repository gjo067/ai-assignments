import gym
import numpy as np
import random
import time
import matplotlib.pyplot as plt

from collections import deque
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam

# Helper function to plot scores, q-values and save it to a picture specified by plot_path.
# Plots the values and their running averages.
def plot_learning(score_history, Q_history, eps_history, plot_path):
    x = [i+1 for i in range(len(score_history))]
    N = len(score_history)
    running_avg_score, running_avg_Q, running_avg_eps = [], [], []
    for t in range(N):
        running_avg_score.append(np.mean(score_history[max(0, t-100):(t+1)]))
        running_avg_Q.append(np.mean(Q_history[max(0,t-100):(t+1)]))
        #running_avg_eps.append(np.mean(eps_history[max(0,t-100):(t+1)]))
    plt.clf()
    plt.plot(x, score_history, "b-")
    #plt.plot(x, eps_history, "k-")
    plt.plot(x, running_avg_score, "g-")
    plt.savefig(plot_path + ".png")
    plt.clf()
    plt.plot(x, Q_history, "b-")
    plt.plot(x, running_avg_Q, "r-")
    plt.savefig(plot_path + "Q.png")

# Class for the DQN Agent
class DQN:
    def __init__(self, env, epsilon=1.0, epsilon_min=0.01, epsilon_decay=0.999):
        print("DQN Agent Created")
        self.memory = deque(maxlen=250000)

        self.state_size = env.observation_space.shape[0]
        # Set of predefined actions.
        #self.actions = [[-0.4, -0.4, -0.4, -0.4], [0.4, 0.4, 0.4, 0.4], [-0.4, -0.4, -0.4, 0.4], [-0.4, -0.4, 0.4, -0.4], [-0.4, 0.4, -0.4, -0.4], [0.4, -0.4, -0.4, -0.4], [-0.4, -0.4, 0.4, 0.4], [-0.4, 0.4, 0.4, -0.4], 
        #                [0.4, 0.4, -0.4, -0.4], [-0.4, 0.4, 0.4, 0.4], [0.4, -0.4, 0.4, 0.4], [0.4, 0.4, -0.4, 0.4], [0.4, 0.4, 0.4, -0.4], [-0.4, 0.4, -0.4, 0.4], [0.4, -0.4, 0.4, -0.4], [0.4, -0.4, -0.4, 0.4]]
        self.actions = create_actions([-0.5, 0, 0.5])
        self.action_size = len(self.actions)

        # parameters.
        self.gamma = 0.99
        self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay
        self.learning_rate = 0.005
        self.batch_size = 32

        self.target_model_update_rate = 10000
        self.trainings_since_last_update = 0

        # Networks.
        self.model = self._build_network()
        self.target_model = self._build_network()
        self.update_target_model()

    # Creates a sequential fully connected network for the agent. {input - 256 - 128 - self.action_size}.
    def _build_network(self):
        model = Sequential()
        model.add(Dense(256, input_dim=self.state_size, activation="relu", kernel_initializer="he_uniform"))
        model.add(Dense(128, activation="relu", kernel_initializer="he_uniform"))
        model.add(Dense(self.action_size, activation="linear"))
        model.compile(loss="mse", optimizer=Adam(lr=self.learning_rate))
        return model

    # Returns the index of the predefined action array of the passed action.
    def _get_action_index(self, action):
        for i in range(len(self.actions)):
            if action == self.actions[i]:
                return i    

    def update_target_model(self):
        self.target_model.set_weights(self.model.get_weights())

    # Remember a transition, changes the action to an index into the predefined action index list.
    def remember(self, state, action, reward, new_state, done):
        self.memory.append((state, self._get_action_index(action), reward, new_state, done))

    # Gets an action with the eps-greedy policy
    def get_action(self, state):
        if np.random.random() <= self.epsilon:
            return self.actions[random.randrange(self.action_size)]
        act_values = self.model.predict(np.reshape(state, [1, self.state_size]))
        return self.actions[np.argmax(act_values[0])]

    # Returns the current best action.
    def get_best_action(self, state):
        return self.actions[np.argmax(self.model.predict(np.reshape(state, [1,self.state_size]))[0])]

    # Returns the max predicted Q value for the current state.
    def get_Q_value(self, state):
        return np.amax(self.model.predict(np.reshape(state, [1, self.state_size]))[0])

    def update_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    # Trains the network on a batch of data. calculating the targets and passing them to the models fit function.
    # Updates the target network if necessary.
    def train(self):
        if len(self.memory) < 5000:
            return
        batch = random.sample(self.memory, self.batch_size)
        state, action, reward, new_state, done = zip(*batch)
        state = np.array(state)
        action = np.array(action)
        reward = np.array(reward)
        new_state = np.array(new_state)
        done = np.array(done, dtype=np.float32)
        for i in range(self.batch_size):
            state[i] = np.reshape(state[i], [1, self.state_size])
            new_state[i] = np.reshape(new_state[i], [1, self.state_size])

        q_eval = self.model.predict(state, batch_size=self.batch_size)
        q_next = self.target_model.predict(new_state)

        q_target = q_eval.copy()

        batch_index = np.arange(self.batch_size, dtype=np.int32)

        for i in range(self.batch_size):
            done[i] = 1 - int(done[i])

        q_target[batch_index, action] = reward + self.gamma * np.max(q_next, axis=1)*done

        _ = self.model.fit(state, q_target, verbose=0)
        
        self.trainings_since_last_update += 1
        if self.trainings_since_last_update >= self.target_model_update_rate:
            self.update_target_model()
            self.trainings_since_last_update = 0

    def save(self, file_path):
        self.model.save_weights(file_path)

    def load(self, file_path):
        self.model.load_weights(file_path)

# Creates a list of all possible distinct action based on a list of possible values in all 4 action values.
def create_actions(possible_values):
    actions = []
    for act1 in possible_values:
        action = [act1, 0, 0, 0]
        for act2 in possible_values:
            action[1] = act2
            for act3 in possible_values:
                action[2] = act3
                for act4 in possible_values:
                    action[3] = act4
                    actions.append(action.copy())
    return actions

if __name__ == "__main__":
    env = gym.make("BipedalWalkerHardcore-v2")

    do_render = True
    do_train = True

    # Create the agent, specify filepaths for weihts and plots.
    agent = DQN(env, epsilon=1.0, epsilon_min=0.05, epsilon_decay=0.999)
    weight_path = "dqn_weights.h5"
    plot_path = "dqn_plot"

    # Load in weights for the network.
    agent.load(weight_path)

    score_history = []
    Q_history = []
    eps_history = []
    max_episodes = 5000
    max_steps = 500

    for ep in range(max_episodes):
        state = env.reset()
        ep_score = 0
        Q_history.append(agent.get_Q_value(state))
        eps_history.append(agent.epsilon)
        start = time.time()

        for step in range(max_steps):
            if do_render:
                env.render()

            if do_train:
                action = agent.get_action(state)
            else:
                action = agent.get_best_action(state)

            state_, reward, done, _ = env.step(action)

            ep_score += reward

            if do_train:
                agent.remember(state, action, reward, state_, done)
                agent.train()
            
            state = state_

            if done:
                break

        agent.update_epsilon()

        score_history.append(ep_score)

        if ep % 100 == 0 and ep > 0:
            agent.save(weight_path)
            plot_learning(score_history, Q_history, eps_history, plot_path)

        ep_time = time.time() - start
        print("Episode: ", ep, " finished, episode score: %.2f " % ep_score, " epsilon: %.3f" % agent.epsilon, " ep_time: %.5f" % ep_time, " max_q: ", Q_history[len(Q_history) - 1])

    plot_learning(score_history, Q_history, eps_history, plot_path)