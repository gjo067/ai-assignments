import random
import numpy as np
import matplotlib.pyplot as plt

# The environment for the luggage game.
class LuggageGameEnv:

    def __init__(self, p1_luggage, p2_luggage, capacity):
        self.p1_luggage = p1_luggage
        self.p2_luggage = p2_luggage
        self.p1_state = self.p1_luggage.copy()
        self.p2_state = self.p2_luggage.copy()
        self.capacity = capacity

    # Reset the environment, setting the states back to the original values.
    def reset(self):
        self.p1_state = self.p1_luggage.copy()
        self.p2_state = self.p2_luggage.copy()

        return self.p1_state.copy(), self.p2_state.copy()

    # Take in actions for p1 and p2. Calculate and return the states, rewards and wether the game is done or not.
    def step(self, p1_action, p2_action):
        for luggage in p1_action:
            try:
                self.p1_state.remove(luggage)
            except ValueError:
                print("Error:", p1_action)

        for luggage in p2_action:
            try:
                self.p2_state.remove(luggage)
            except ValueError:
                print("Error:", p2_action)

        p1_reward, p2_reward = self._get_rewards(p1_action, p2_action)
        done = (p1_reward == -1000) or (p2_reward == -1000) or (len(self.p1_state) == 0 and len(self.p2_state) == 0)

        return self.p1_state.copy(), self.p2_state.copy(), p1_reward, p2_reward, done

    # Helper function to calculate reward based on the players actions.
    def _get_rewards(self, p1_action, p2_action):
        total_weight = 100 # Captain is 100 kg
        p1_reward = p2_reward = 0
        # Is passenger onboard?
        p1_onboard = len(p1_action) > 0
        p2_onboard = len(p2_action) > 0

        if p1_onboard:
            p1_reward = -100*(1+min(len(self.p1_state),1))+10*sum(p1_action)
            total_weight += sum(p1_action) + 100
        if p2_onboard:
            p2_reward = -100*(1+min(len(self.p2_state),1))+10*sum(p2_action)
            total_weight += sum(p2_action) + 100

        #p1_reward -= sum(self.p1_state) * 4
        #p2_reward -= sum(self.p2_state) * 4

        #if p1_onboard and len(self.p1_state) == 0:
        #    p1_reward += 10000
        #if p2_onboard and len(self.p2_state) == 0:
        #    p2_reward += 10000

        # If the boat sinks, give a reward of -1000 to every passenger onboard.
        if total_weight > self.capacity:
            if p1_onboard: p1_reward = -1000
            else: p1_reward = 0
            if p2_onboard: p2_reward = -1000
            else: p2_reward = 0

        return p1_reward, p2_reward

# Create a table where only valid actions have value 0, invalid actions have value None
def create_valid_q_table(states, actions, full_state=False):
    table = np.full(shape=(len(states), len(actions)), fill_value=None)

    for s_idx in range(len(states)):
        for a_idx in range(len(actions)):
            legal = True
            for item in actions[a_idx]:
                states_item_count = states[s_idx][0].count(item) if full_state else states[s_idx].count(item)
                if states_item_count < actions[a_idx].count(item):
                    legal = False
                    break
            if legal:
                table[s_idx][a_idx] = 0
            #if set(actions[a_idx]).issubset(states[s_idx]):
            #    table[s_idx][a_idx] = 0
        # Sets actions of doing nothing to be illegal, uncomment to allow.
        if s_idx is not 0:
            table[s_idx][0] = None

    if full_state:
        idx = 0
        while states[idx][0] == []:
            table[idx][0] = 0
            idx += 1
    else:
        table[0][0] = 0

    #print(table)
    return table

# A simple agent using a Q-table to find the best action.
class QAgent:
    def __init__(self, states, actions, lr, gamma, q_table):
        self.states = states
        self.actions = actions
        self.lr = lr
        self.gamma = gamma
        self.Q = q_table

    # Helper functions to turn lists of states and actions into indices for the q-table.
    def _get_state_index(self, state):
        for i in range(len(self.states)):
            if self.states[i] == state:
                return i

    def _get_action_index(self, action):
        for i in range(len(self.actions)):
            if self.actions[i] == action:
                return i

    def _get_best_action_idxs(self, s_idx):
        best_q_value = -10000
        best_idxs = []

        for i in range(len(self.Q[s_idx])):
            cur_q = self.Q[s_idx][i]
            if cur_q == None:
                continue
            elif cur_q == best_q_value:
                best_idxs.append(i)
            elif cur_q > best_q_value:
                best_q_value = cur_q
                best_idxs = [i]

        return best_idxs, best_q_value

    # Returns the best action, if more than one state has the same best value, return one of them at random.
    def get_best_action(self, state):
        s_idx = self._get_state_index(state)
        a_idxs, _ = self._get_best_action_idxs(s_idx)
        return self.actions[random.choice(a_idxs)].copy()

    # Returns a random legal action for the given state.
    def get_random_action(self, state):
        s_idx = self._get_state_index(state)
        valid_actions = []
        for i in range(len(self.actions)):
            if self.Q[s_idx][i] is not None:
                valid_actions.append(self.actions[i])
        return random.choice(valid_actions).copy()

    # If random < epsilon, return random action, else return the current best action.
    def get_action_eps_greedy(self, state, epsilon):
        if random.random() < epsilon:
            return self.get_random_action(state)
        else:
            return self.get_best_action(state)

    # Update q-table with values from the latest transition.
    def update_table(self, state, action, reward, new_state):
        s_idx = self._get_state_index(state)
        a_idx = self._get_action_index(action)
        n_s_idx = self._get_state_index(new_state)
        old_state_q_value = self.Q[s_idx][a_idx]
        _, max_new_state_q_value = self._get_best_action_idxs(n_s_idx)
        self.Q[s_idx][a_idx] = old_state_q_value + self.lr * \
            (reward + self.gamma * max_new_state_q_value - old_state_q_value)
        #self.Q[s_idx][a_idx] = (1 - self.lr) * old_state_q_value + self.lr * (reward + self.gamma * max_new_state_q_value)

# Code for playing task A. Only playes the first episode of every game.
def task_a(p1_luggage_combinations, p2_luggage_combinations, lr, gamma, epsilon, epsilon_decay, epsilon_min, num_episodes, verbose=False):
    p1_score_history = []
    p2_score_history = []

    env = LuggageGameEnv([20, 30, 80, 100], [30, 50, 50, 90], 500)

    p1 = QAgent(p1_luggage_combinations, p1_luggage_combinations, lr, gamma, create_valid_q_table(p1_luggage_combinations, p1_luggage_combinations))
    p2 = QAgent(p2_luggage_combinations, p2_luggage_combinations, lr, gamma, create_valid_q_table(p2_luggage_combinations, p2_luggage_combinations))

    for ep in range(num_episodes):
        #if ep - num_episodes > -100:
        #    verbose = True
        p1_state, p2_state = env.reset()
        p1_score = p2_score = 0
        done = False

        if verbose:
            print("New game started:")

        p1_action = p1.get_action_eps_greedy(p1_state, epsilon)
        p2_action = p2.get_action_eps_greedy(p2_state, epsilon)

        new_p1_state, new_p2_state, p1_reward, p2_reward, done = env.step(p1_action, p2_action)

        if verbose:
            print("Passenger 1 brought onboard: ", p1_action, " and got reward: ", p1_reward, " and has remaining luggage: ", new_p1_state)
            print("Passenger 2 brought onboard: ", p2_action, " and got reward: ", p2_reward, " and has remaining luggage: ", new_p2_state)
            if done and (p1_reward == -1000 or p2_reward == -1000):
                print("Boat sunk")

        p1_score += p1_reward
        p2_score += p2_reward

        p1.update_table(p1_state, p1_action, p1_reward, new_p1_state)
        p2.update_table(p2_state, p2_action, p2_reward, new_p2_state)

        print("Task A: Episode ", ep, " finished")

        p1_score_history.append(p1_score)
        p2_score_history.append(p2_score)
        epsilon = max(epsilon * epsilon_decay, epsilon_min)

    #print("p1.Q")
    #print(p1.Q)
    #print("p2.Q")
    #print(p2.Q)

    return p1_score_history, p2_score_history

# Function for playing task B.
def task_b(p1_luggage_combinations, p2_luggage_combinations, lr, gamma, epsilon, epsilon_decay, epsilon_min, num_episodes, verbose=False):
    p1_score_history = []
    p2_score_history = []

    env = LuggageGameEnv([20, 30, 80, 100], [30, 50, 50, 90], 500)

    p1 = QAgent(p1_luggage_combinations, p1_luggage_combinations, lr, gamma, create_valid_q_table(p1_luggage_combinations, p1_luggage_combinations))
    p2 = QAgent(p2_luggage_combinations, p2_luggage_combinations, lr, gamma, create_valid_q_table(p2_luggage_combinations, p2_luggage_combinations))

    for ep in range(num_episodes):
        #if ep - num_episodes > -100:
        #    verbose = True
        p1_state, p2_state = env.reset()
        p1_score = p2_score = 0
        done = False

        if verbose:
            print("New game started:")

        while not done:
            p1_action = p1.get_action_eps_greedy(p1_state, epsilon)
            p2_action = p2.get_action_eps_greedy(p2_state, epsilon)

            new_p1_state, new_p2_state, p1_reward, p2_reward, done = env.step(p1_action, p2_action)

            if verbose:
                print("Passenger 1 brought onboard: ", p1_action, " and got reward: ", p1_reward, " and has remaining luggage: ", new_p1_state)
                print("Passenger 2 brought onboard: ", p2_action, " and got reward: ", p2_reward, " and has remaining luggage: ", new_p2_state)
                if done and (p1_reward == -1000 or p2_reward == -1000):
                    print("Boat sunk")
            
            p1_score += p1_reward
            p2_score += p2_reward

            p1.update_table(p1_state, p1_action, p1_reward, new_p1_state)
            p2.update_table(p2_state, p2_action, p2_reward, new_p2_state)

            p1_state = new_p1_state
            p2_state = new_p2_state

        print("Task B: Episode ", ep, " finished")

        p1_score_history.append(p1_score)
        p2_score_history.append(p2_score)
        epsilon = max(epsilon * epsilon_decay, epsilon_min)

    #print("p1.Q")
    #print(p1.Q)
    #print("p2.Q")
    #print(p2.Q)

    return p1_score_history, p2_score_history

# Function for playing task C
def task_c(p1_luggage_combinations, p2_luggage_combinations, lr, gamma, epsilon, epsilon_decay, epsilon_min, num_episodes, verbose=False):
    p1_score_history = []
    p2_score_history = []

    p1_states = []
    for state1 in p1_luggage_combinations:
        for state2 in p2_luggage_combinations:
            p1_states.append([state1, state2])
    
    p2_states = []
    for state2 in p2_luggage_combinations:
        for state1 in p1_luggage_combinations:
            p2_states.append([state2, state1])

    env = LuggageGameEnv([20, 30, 80, 100], [30, 50, 50, 90], 500)

    p1 = QAgent(p1_states, p1_luggage_combinations, lr, gamma, create_valid_q_table(p1_states, p1_luggage_combinations, True))
    p2 = QAgent(p2_states, p2_luggage_combinations, lr, gamma, create_valid_q_table(p2_states, p2_luggage_combinations, True))

    for ep in range(num_episodes):
        #if ep - num_episodes > -100:
        #    verbose = True
        p1_state, p2_state = env.reset()
        p1_score = p2_score = 0
        done = False

        if verbose:
            print("New game started:")

        while not done:
            p1_action = p1.get_action_eps_greedy([p1_state, p2_state], epsilon)
            p2_action = p2.get_action_eps_greedy([p2_state, p1_state], epsilon)

            new_p1_state, new_p2_state, p1_reward, p2_reward, done = env.step(p1_action, p2_action)

            if verbose:
                print("Passenger 1 brought onboard: ", p1_action, " and got reward: ", p1_reward, " and has remaining luggage: ", new_p1_state)
                print("Passenger 2 brought onboard: ", p2_action, " and got reward: ", p2_reward, " and has remaining luggage: ", new_p2_state)
                if done and (p1_reward == -1000 or p2_reward == -1000):
                    print("Boat sunk")
            
            p1_score += p1_reward
            p2_score += p2_reward

            p1.update_table([p1_state, p2_state], p1_action, p1_reward, [new_p1_state, new_p2_state])
            p2.update_table([p2_state, p1_state], p2_action, p2_reward, [new_p2_state, new_p1_state])

            p1_state = new_p1_state
            p2_state = new_p2_state

        print("Task C: Episode ", ep, " finished")

        p1_score_history.append(p1_score)
        p2_score_history.append(p2_score)
        epsilon = max(epsilon * epsilon_decay, epsilon_min)

    #print("p1.Q")
    #print(p1.Q)
    #print("p2.Q")
    #print(p2.Q)

    return p1_score_history, p2_score_history

if __name__ == "__main__":
    # Possible luggage combinations for each passenger.
    p1_luggage_combinations = [[], [20], [30], [80], [100], [20, 30], [20, 80], [20, 100], [30, 80], [30, 100], [80, 100], [20, 30, 80], [20, 30, 100], [20, 80, 100], [30, 80, 100], [20, 30, 80, 100]] # 16
    p2_luggage_combinations = [[], [30], [50], [90], [30, 50], [30, 90], [50, 50], [50, 90], [30, 50, 50], [30, 50, 90], [50, 50, 90], [30, 50, 50, 90]] # 12

    # Parameters
    lr = 0.005
    gamma = 0.95
    epsilon = 1.0
    epsilon_decay = 0.995
    epsilon_min = 0.0
    num_episodes = 2000

    np.random.seed(0)

    # Plays a game for a specified task, use task_{a,b,c}() to play the different games.
    p1_scores, p2_scores = task_c(p1_luggage_combinations, p2_luggage_combinations, lr, gamma, 
                                    epsilon, epsilon_decay, epsilon_min, num_episodes, False)

    print("Average p1_scores: ", (sum(p1_scores) / len(p1_scores)))
    print("Average p2_scores: ", (sum(p2_scores) / len(p2_scores)))

    # Plots running avg scores for each player for all games.
    x = [i+1 for i in range(num_episodes)]
    N = len(p1_scores)
    p1_running_avg = []
    p2_running_avg = []
    for t in range(N):
	    p1_running_avg.append(np.mean(p1_scores[max(0, t-100):(t+1)]))
	    p2_running_avg.append(np.mean(p2_scores[max(0, t-100):(t+1)]))
    #p1_scores = p1_scores[-1000:len(p1_scores)]
    #p2_scores = p2_scores[-1000:len(p2_scores)]
    plt.plot(x, p1_running_avg)
    plt.plot(x, p2_running_avg)

    plt.legend(["p1", "p2"])

    plt.savefig("ass2.png")