import os
import tensorflow as tf
import numpy as np
from tensorflow.initializers import random_uniform
import matplotlib.pyplot as plt
import gym
import time

# Code from https://github.com/philtabor/Youtube-Code-Repository/blob/master/ReinforcementLearning/PolicyGradient/DDPG/walker2d/tensorflow/ddpg_orig_tf.py
# With some modifications, like adding the model, and approximating it.

# Agent following the walking from the github of the example
class WalkingAgent:
    def __init__(self):
        self.reset()

    def reset(self):
        self.a = np.array([0.0, 0.0, 0.0, 0.0])
        self.STAY_ON_ONE_LEG, self.PUT_OTHER_DOWN, self.PUSH_OFF = 1,2,3
        self.SPEED = 0.29  # Will fall forward on higher speed
        self.state = self.STAY_ON_ONE_LEG
        self.moving_leg = 0
        self.supporting_leg = 1 - self.moving_leg
        self.SUPPORT_KNEE_ANGLE = +0.1
        self.supporting_knee_angle = self.SUPPORT_KNEE_ANGLE

    def get_best_action(self, state):
        contact0 = state[8]
        contact1 = state[13]
        moving_s_base = 4 + 5*self.moving_leg
        supporting_s_base = 4 + 5*self.supporting_leg

        hip_targ  = [None,None]   # -0.8 .. +1.1
        knee_targ = [None,None]   # -0.6 .. +0.9
        hip_todo  = [0.0, 0.0]
        knee_todo = [0.0, 0.0]

        if self.state==self.STAY_ON_ONE_LEG:
            hip_targ[self.moving_leg]  = 1.1
            knee_targ[self.moving_leg] = -0.6
            self.supporting_knee_angle += 0.03
            if state[2] > self.SPEED: self.supporting_knee_angle += 0.03
            self.supporting_knee_angle = min( self.supporting_knee_angle, self.SUPPORT_KNEE_ANGLE )
            knee_targ[self.supporting_leg] = self.supporting_knee_angle
            if state[supporting_s_base+0] < 0.10: # supporting leg is behind
                self.state = self.PUT_OTHER_DOWN
        if self.state==self.PUT_OTHER_DOWN:
            hip_targ[self.moving_leg]  = +0.1
            knee_targ[self.moving_leg] = self.SUPPORT_KNEE_ANGLE
            knee_targ[self.supporting_leg] = self.supporting_knee_angle
            if state[moving_s_base+4]:
                self.state = self.PUSH_OFF
                self.supporting_knee_angle = min( state[moving_s_base+2], self.SUPPORT_KNEE_ANGLE )
        if self.state==self.PUSH_OFF:
            knee_targ[self.moving_leg] = self.supporting_knee_angle
            knee_targ[self.supporting_leg] = +1.0
            if state[supporting_s_base+2] > 0.88 or state[2] > 1.2*self.SPEED:
                self.state = self.STAY_ON_ONE_LEG
                self.moving_leg = 1 - self.moving_leg
                self.supporting_leg = 1 - self.moving_leg

        if hip_targ[0]: hip_todo[0] = 0.9*(hip_targ[0] - state[4]) - 0.25*state[5]
        if hip_targ[1]: hip_todo[1] = 0.9*(hip_targ[1] - state[9]) - 0.25*state[10]
        if knee_targ[0]: knee_todo[0] = 4.0*(knee_targ[0] - state[6])  - 0.25*state[7]
        if knee_targ[1]: knee_todo[1] = 4.0*(knee_targ[1] - state[11]) - 0.25*state[12]

        hip_todo[0] -= 0.9*(0-state[0]) - 1.5*state[1] # PID to keep head strait
        hip_todo[1] -= 0.9*(0-state[0]) - 1.5*state[1]
        knee_todo[0] -= 15.0*state[3]  # vertical speed, to damp oscillations
        knee_todo[1] -= 15.0*state[3]

        self.a[0] = hip_todo[0]
        self.a[1] = knee_todo[0]
        self.a[2] = hip_todo[1]
        self.a[3] = knee_todo[1]
        self.a = np.clip(0.5*self.a, -1.0, 1.0)

        return self.a

# Helper function for plotting scores and running average score.
def plotLearning(scores, filename, x=None, window=5):   
    N = len(scores)
    running_avg = np.empty(N)
    for t in range(N):
	    running_avg[t] = np.mean(scores[max(0, t-window):(t+1)])
    if x is None:
        x = [i for i in range(N)]
    plt.ylabel('Score')       
    plt.xlabel('Game')       
    plt.plot(x, scores)        
    plt.plot(x, running_avg)
    plt.savefig(filename)

# Noise generator, for exploration
class OUActionNoise(object):
    def __init__(self, mu, sigma=0.15, theta=0.2, dt=1e-2, x0=None):
        self.theta = theta
        self.sigma = sigma
        self.mu = mu
        self.dt = dt
        self.x0 = x0
        self.reset()

    def __call__(self):
        x = self.x_prev + self.theta *(self.mu-self.x_prev)*self.dt + self.sigma*np.sqrt(self.dt)*np.random.normal(size=self.mu.shape)
        self.x_prev = x
        return x

    def reset(self):
        self.x_prev = self.x0 if self.x0 is not None else np.zeros_like(self.mu)

# Replay Memory. Stores every transition, used to sample random transitions for the training.
class ReplayBuffer(object):
    def __init__(self, max_size, input_shape, n_actions):
        self.mem_size = max_size
        self.mem_cntr = 0
        self.state_memory = np.zeros((self.mem_size, *input_shape))
        self.new_state_memory = np.zeros((self.mem_size, *input_shape))
        self.action_memory = np.zeros((self.mem_size, n_actions))
        self.reward_memory = np.zeros(self.mem_size)
        self.terminal_memory = np.zeros(self.mem_size, dtype=np.float32)

    def store_transition(self, state, action, reward, new_state, done):
        index = self.mem_cntr % self.mem_size
        self.state_memory[index] = state
        self.new_state_memory[index] = new_state
        self.action_memory[index] = action
        self.reward_memory[index] = reward
        self.terminal_memory[index] = 1 - int(done)
        self.mem_cntr += 1

    def sample_buffer(self, batch_size):
        max_mem = min(self.mem_size, self.mem_cntr)
        batch = np.random.choice(max_mem, batch_size)

        states = self.state_memory[batch]
        new_states = self.new_state_memory[batch]
        actions = self.action_memory[batch]
        rewards = self.reward_memory[batch]
        terminals = self.terminal_memory[batch]

        return states, actions, rewards, new_states, terminals

# The actor network. Used for predicting actions
class Actor(object):
    def __init__(self, lr, n_actions, name, input_dims, sess, fc1_dims, fc2_dims, action_bound, batch_size=64, chkpt_dir="tmp\\ddpg_model"):
        self.lr = lr
        self.n_actions = n_actions
        self.name = name
        self.input_dims = input_dims
        self.sess = sess
        self.fc1_dims = fc1_dims
        self.fc2_dims = fc2_dims
        self.batch_size = batch_size
        self.action_bound = action_bound
        self.build_network()
        self.params = tf.trainable_variables(scope=self.name)
        self.saver = tf.train.Saver()
        self.checkpoint_file = os.path.join(chkpt_dir, name+"_ddpg.ckpt")

        self.unnormalized_actor_gradients = tf.gradients(self.mu, self.params, -self.action_gradient)
        self.actor_gradients = list(map(lambda x: tf.div(x, self.batch_size), self.unnormalized_actor_gradients))
        self.optimize = tf.train.AdamOptimizer(self.lr).apply_gradients(zip(self.actor_gradients, self.params))

    def build_network(self):
        with tf.variable_scope(self.name):
            self.input = tf.placeholder(tf.float32, shape=[None, *self.input_dims], name="inputs")
            self.action_gradient = tf.placeholder(tf.float32, shape=[None, self.n_actions], name="gradients")

            f1 = 1. / np.sqrt(self.fc1_dims)
            dense1 = tf.layers.dense(self.input, units=self.fc1_dims, kernel_initializer=random_uniform(-f1, f1), bias_initializer=random_uniform(-f1, f1))
            batch1 = tf.layers.batch_normalization(dense1)
            layer1_activation = tf.nn.relu(batch1)

            f2 = 1. / np.sqrt(self.fc2_dims)
            dense2 = tf.layers.dense(layer1_activation, units=self.fc2_dims, kernel_initializer=random_uniform(-f2, f2), bias_initializer=random_uniform(-f2, f2))
            batch2 = tf.layers.batch_normalization(dense2)
            layer2_activation = tf.nn.relu(batch2)

            f3 = 0.003
            mu = tf.layers.dense(layer2_activation, units=self.n_actions, activation="tanh", kernel_initializer=random_uniform(-f3, f3), bias_initializer=random_uniform(-f3, f3))
            self.mu = tf.multiply(mu, self.action_bound)

    def predict(self, inputs):
        return self.sess.run(self.mu, feed_dict={self.input: inputs})

    def train(self, inputs, gradients):
        self.sess.run(self.optimize, feed_dict={self.input: inputs, self.action_gradient: gradients})

    def load_checkpoint(self):
        print("... loading checkpoint ...")
        self.saver.restore(self.sess, self.checkpoint_file)

    def save_checkpoint(self):
        print("... saving checkpoint ...")
        self.saver.save(self.sess, self.checkpoint_file)

# Critic network, used for predicting action-values, used for calculating targets and training the networks.
class Critic(object):
    def __init__(self, lr, n_actions, name, input_dims, sess, fc1_dims, fc2_dims, batch_size=64, chkpt_dir="tmp\\ddpg_model"):
        self.lr = lr
        self.n_actions = n_actions
        self.name = name
        self.fc1_dims = fc1_dims
        self.fc2_dims = fc2_dims
        self.input_dims = input_dims
        self.batch_size = batch_size
        self.sess = sess
        self.build_network()
        self.params = tf.trainable_variables(scope=self.name)
        self.saver = tf.train.Saver()
        self.checkpoint_file = os.path.join(chkpt_dir, name+"_ddpg.ckpt")

        self.optimize = tf.train.AdamOptimizer(self.lr).minimize(self.loss)
        self.action_gradients = tf.gradients(self.q, self.actions)

    def build_network(self):
        with tf.variable_scope(self.name):
            self.input = tf.placeholder(tf.float32, shape=[None, *self.input_dims], name="inputs")
            self.actions = tf.placeholder(tf.float32, shape=[None, self.n_actions], name="actions")
            self.q_target = tf.placeholder(tf.float32, shape=[None, 1], name="targets")

            f1 = 1. / np.sqrt(self.fc1_dims)
            dense1 = tf.layers.dense(self.input, units=self.fc1_dims, kernel_initializer=random_uniform(-f1, f1), bias_initializer=random_uniform(-f1, f1))
            batch1 = tf.layers.batch_normalization(dense1)
            layer1_activation = tf.nn.relu(batch1)

            f2 = 1. / np.sqrt(self.fc2_dims)
            dense2 = tf.layers.dense(layer1_activation, units=self.fc2_dims, kernel_initializer=random_uniform(-f2, f2), bias_initializer=random_uniform(-f2, f2))
            batch2 = tf.layers.batch_normalization(dense2)
            
            action_in = tf.layers.dense(self.actions, units=self.fc2_dims, activation="relu")

            state_actions = tf.add(batch2, action_in)
            state_actions = tf.nn.relu(state_actions)

            f3 = 0.003
            self.q = tf.layers.dense(state_actions, units=1, kernel_initializer=random_uniform(-f3, f3), bias_initializer=random_uniform(-f3, f3), kernel_regularizer=tf.keras.regularizers.l2(0.01))

            self.loss = tf.losses.mean_squared_error(self.q_target, self.q)

    def predict(self, inputs, actions):
        return self.sess.run(self.q, feed_dict={self.input: inputs, self.actions: actions})

    def train(self, inputs, actions, q_target):
        return self.sess.run(self.optimize, feed_dict={self.input: inputs, self.actions: actions, self.q_target: q_target})

    # Create gradients for actor to train on
    def get_action_gradients(self, inputs, actions):
        return self.sess.run(self.action_gradients, feed_dict={self.input: inputs, self.actions: actions})

    def load_checkpoint(self):
        print("... loading checkpoint ...")
        self.saver.restore(self.sess, self.checkpoint_file)

    def save_checkpoint(self):
        print("... saving checkpoint ...")
        self.saver.save(self.sess, self.checkpoint_file)

# The DDPG Agent. Contains an actor and a critic network, and target networks for both.
# Uses a model for gathering training data in the beginning.
class Agent(object):
    def __init__(self, alpha, beta, input_dims, tau, env, gamma=0.99, n_actions=2, max_size=1000000, layer1_size=400, layer2_size=300, batch_size=64, chkpt_dir="tmp\\ddpg_model", use_gpu=False, use_model=False):
        self.gamma = gamma
        self.tau = tau
        self.memory = ReplayBuffer(max_size, input_dims, n_actions)
        self.batch_size = batch_size
        
        if use_gpu:
            self.sess = tf.Session()
        else:
            config = tf.ConfigProto(device_count={"GPU":0})
            self.sess = tf.Session(config=config)

        # Model to kickstart learning
        self.use_model = use_model
        self.model = WalkingAgent()
        self.steps_with_model = 250000
        self.cur_steps_with_model = 0
        self.model_critic = Critic(0.1, n_actions, "ModelCritic", input_dims, self.sess, layer1_size, layer2_size)
        self.model_actor = Actor(0.1, n_actions, "ModelActor", input_dims, self.sess, layer1_size, layer2_size, env.action_space.high)

        self.actor = Actor(alpha, n_actions, "Actor", input_dims, self.sess, layer1_size, layer2_size, env.action_space.high, batch_size=batch_size, chkpt_dir=chkpt_dir)
        self.target_actor = Actor(alpha, n_actions, "TargetActor", input_dims, self.sess, layer1_size, layer2_size, env.action_space.high, batch_size=batch_size, chkpt_dir=chkpt_dir)

        self.critic = Critic(beta, n_actions, "Critic", input_dims, self.sess, layer1_size, layer2_size, chkpt_dir=chkpt_dir)
        self.target_critic = Critic(beta, n_actions, "TargetCritic", input_dims, self.sess, layer1_size, layer2_size, chkpt_dir=chkpt_dir)

        self.noise = OUActionNoise(mu=np.zeros(n_actions))

        
        self.update_critic = \
            [self.target_critic.params[i].assign(tf.multiply(self.critic.params[i], self.tau)+tf.multiply(self.target_critic.params[i], 1. - self.tau))
            for i in range(len(self.target_critic.params))]

        self.update_actor = \
            [self.target_actor.params[i].assign(tf.multiply(self.actor.params[i], self.tau) + tf.multiply(self.target_actor.params[i], 1. - self.tau))
            for i in range(len(self.target_actor.params))]

        self.sess.run(tf.global_variables_initializer())
        self.update_network_parameters(first=True)
    
    # Functions for updating the target network's parameters
    # tw_1 = (1 - tau) * tw_1 + tau * w_1
    def update_network_parameters(self, first=False):
        if first:
            old_tau = self.tau
            self.tau = 1.0
            self.target_critic.sess.run(self.update_critic)
            self.target_actor.sess.run(self.update_actor)
            self.tau = old_tau
        else:
            self.target_critic.sess.run(self.update_critic)
            self.target_actor.sess.run(self.update_actor)

    def remember(self, state, action, reward, new_state, done):
        self.memory.store_transition(state, action, reward, new_state, done)

    def choose_action(self, state):
        if self.use_model:
            if self.cur_steps_with_model < self.steps_with_model:
                self.cur_steps_with_model += 1
                return self.model.get_best_action(state)
            elif self.cur_steps_with_model == self.steps_with_model:
                self.cur_steps_with_model += 1
                for i in range(len(self.model_actor.params)):
                    self.actor.params[i] = self.model_actor.params[i]
                for j in range(len(self.model_critic.params)):
                    self.critic.params[j] = self.model_critic.params[j]
                self.update_network_parameters(first=True)

        state = state[np.newaxis, :]
        mu = self.actor.predict(state) # returns list of list

        mu_prime = mu# + self.noise()

        return mu_prime[0]

    def learn(self):
        if self.memory.mem_cntr < self.batch_size:
            return

        if self.use_model:
            if self.cur_steps_with_model < self.steps_with_model:
                self.learn_with_model()
                return

        state, action, reward, new_state, done = self.memory.sample_buffer(self.batch_size)

        critic_value_ = self.target_critic.predict(new_state, self.target_actor.predict(new_state))

        target = []
        for j in range(self.batch_size):
            target.append(reward[j] + self.gamma * critic_value_[j]*done[j])
        target = np.reshape(target, (self.batch_size, 1))

        _ = self.critic.train(state, action, target)

        a_outs = self.actor.predict(state)
        grads = self.critic.get_action_gradients(state, a_outs)
        self.actor.train(state, grads[0])

        self.update_network_parameters()

    def learn_with_model(self):
        state, action, reward, new_state, done = self.memory.sample_buffer(self.batch_size)

        critic_value = self.model_critic.predict(new_state, self.model_actor.predict(new_state))

        target = []
        for j in range(self.batch_size):
            target.append(reward[j] + self.gamma * critic_value[j]*done[j])
        target = np.reshape(target, (self.batch_size, 1))

        _ = self.model_critic.train(state, action, target)

        a_outs = self.model_actor.predict(state)
        grads = self.model_critic.get_action_gradients(state, a_outs)
        self.model_actor.train(state, grads[0])

    def save_models(self):
        self.actor.save_checkpoint()
        self.target_actor.save_checkpoint()
        self.critic.save_checkpoint()
        self.target_critic.save_checkpoint()

    def load_models(self):
        self.actor.load_checkpoint()
        self.target_actor.load_checkpoint()
        self.critic.load_checkpoint()
        self.target_critic.load_checkpoint()

if __name__ == "__main__":
    #os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    #os.environ["CUDA_VISIBLE_DEVICES"] = "0"

    env = gym.make("BipedalWalkerHardcore-v2")

    agent = Agent(alpha=0.0001, beta=0.001, input_dims=[24], tau=0.001, env=env, n_actions=4, max_size=1000000, batch_size=64, use_gpu=True, use_model=False)
    np.random.seed(0)

    agent.load_models()

    score_history = []
    total_time = 0
    
    for i in range(5000):
        obs = env.reset()
        if agent.cur_steps_with_model < agent.steps_with_model:
            agent.model.reset()
        done = False
        score = 0
        start = time.time()
        for _ in range(500):
            env.render()
            act = agent.choose_action(obs)
            new_state, reward, done, info = env.step(act)
            agent.remember(obs, act, reward, new_state, int(done))
            #agent.learn()
            score += reward
            obs = new_state

            if done:
                break

        score_history.append(score)
        ep_time = time.time() - start
        total_time += ep_time
        print("episode ", i, "score %.2f" % score, 
            "trailing 100 games avg %.2f" % np.mean(score_history[-100:]),
            "elapsed time: %.5f seconds" % ep_time,
            "Avg time: %.5f seconds" % (total_time / (i+1)))

        if i % 100 == 0 and i > 0:
            agent.save_models()
            plotLearning(score_history, "bipedal_model.png", window=100)

    filename="bipedal_model.png"
    plotLearning(score_history, filename, window=100)