import os
import gym
import random
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

from collections import deque
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation
from keras.optimizers import Adam
from keras import backend as K
from tensorflow.initializers import random_uniform

##############################################################################
#
#   Agent that only takes random actions
#
##############################################################################

# Agent choosing purely random actions from the environment
class RandomAgent:
    def __init__(self, env):
        self.env = env

    def get_best_action(self, state):
        return env.action_space.sample()

##################################################################################
#   Agent that uses hard coded instructions to walk
#   Code taken from the github page of the BipedalWalker environment:
#   https://github.com/openai/gym/blob/master/gym/envs/box2d/bipedal_walker.py
##################################################################################

# Agent following the walking from the github of the example
class WalkingAgent:
    def __init__(self):
        self.a = np.array([0.0, 0.0, 0.0, 0.0])
        self.STAY_ON_ONE_LEG, self.PUT_OTHER_DOWN, self.PUSH_OFF = 1,2,3
        self.SPEED = 0.29  # Will fall forward on higher speed
        self.state = self.STAY_ON_ONE_LEG
        self.moving_leg = 0
        self.supporting_leg = 1 - self.moving_leg
        self.SUPPORT_KNEE_ANGLE = +0.1
        self.supporting_knee_angle = self.SUPPORT_KNEE_ANGLE

    def get_best_action(self, state):
        contact0 = state[8]
        contact1 = state[13]
        moving_s_base = 4 + 5*self.moving_leg
        supporting_s_base = 4 + 5*self.supporting_leg

        hip_targ  = [None,None]   # -0.8 .. +1.1
        knee_targ = [None,None]   # -0.6 .. +0.9
        hip_todo  = [0.0, 0.0]
        knee_todo = [0.0, 0.0]

        if self.state==self.STAY_ON_ONE_LEG:
            hip_targ[self.moving_leg]  = 1.1
            knee_targ[self.moving_leg] = -0.6
            self.supporting_knee_angle += 0.03
            if state[2] > self.SPEED: self.supporting_knee_angle += 0.03
            self.supporting_knee_angle = min( self.supporting_knee_angle, self.SUPPORT_KNEE_ANGLE )
            knee_targ[self.supporting_leg] = self.supporting_knee_angle
            if state[supporting_s_base+0] < 0.10: # supporting leg is behind
                self.state = self.PUT_OTHER_DOWN
        if self.state==self.PUT_OTHER_DOWN:
            hip_targ[self.moving_leg]  = +0.1
            knee_targ[self.moving_leg] = self.SUPPORT_KNEE_ANGLE
            knee_targ[self.supporting_leg] = self.supporting_knee_angle
            if state[moving_s_base+4]:
                self.state = self.PUSH_OFF
                self.supporting_knee_angle = min( state[moving_s_base+2], self.SUPPORT_KNEE_ANGLE )
        if self.state==self.PUSH_OFF:
            knee_targ[self.moving_leg] = self.supporting_knee_angle
            knee_targ[self.supporting_leg] = +1.0
            if state[supporting_s_base+2] > 0.88 or state[2] > 1.2*self.SPEED:
                self.state = self.STAY_ON_ONE_LEG
                self.moving_leg = 1 - self.moving_leg
                self.supporting_leg = 1 - self.moving_leg

        if hip_targ[0]: hip_todo[0] = 0.9*(hip_targ[0] - state[4]) - 0.25*state[5]
        if hip_targ[1]: hip_todo[1] = 0.9*(hip_targ[1] - state[9]) - 0.25*state[10]
        if knee_targ[0]: knee_todo[0] = 4.0*(knee_targ[0] - state[6])  - 0.25*state[7]
        if knee_targ[1]: knee_todo[1] = 4.0*(knee_targ[1] - state[11]) - 0.25*state[12]

        hip_todo[0] -= 0.9*(0-state[0]) - 1.5*state[1] # PID to keep head strait
        hip_todo[1] -= 0.9*(0-state[0]) - 1.5*state[1]
        knee_todo[0] -= 15.0*state[3]  # vertical speed, to damp oscillations
        knee_todo[1] -= 15.0*state[3]

        self.a[0] = hip_todo[0]
        self.a[1] = knee_todo[0]
        self.a[2] = hip_todo[1]
        self.a[3] = knee_todo[1]
        self.a = np.clip(0.5*self.a, -1.0, 1.0)

        return self.a

if __name__ == "__main__":
    env = gym.make("BipedalWalkerHardcore-v2")

    # ---- Agent doing random actions, must use do_train = False ----
    #agent = RandomAgent(env)

    # ---- Agent walking using a model, must use do_train = False ----
    agent = WalkingAgent()

    max_episodes = 200
    max_steps = 500

    score_history = []

    for ep in range(max_episodes):
        state = env.reset()
        ep_score = 0

        for step in range(max_steps):
            env.render()
            action = agent.get_best_action(state)
            state_, reward, done, _ = env.step(action)  
            ep_score += reward
            state = state_
            if done:
                break

        score_history.append(ep_score)

        print("Episode ", ep, " finished, episode score = ", ep_score)

    x = [i+1 for i in range(len(score_history))]
    N = len(score_history)
    running_avg = []
    for t in range(N):
        running_avg.append(np.mean(score_history[max(0, t-100):(t+1)]))
    plt.plot(x, score_history)
    plt.plot(x, running_avg)

    plt.savefig("assignment1.png")